fetch("../admin/php/admin.php/fetchusers")

.then(function(response){
    return response.json();
})
.then(function(users){
    let placeholder = document.querySelector("#allUsers");
    let Y = "Admitted"
    let N = "Pending"
    let R = "Rejected"

    let out = "";
    for(let user of users) {
        out += `
            <tr>
                <td>${user.id}</td>
                <td>${user.firstname}  ${user.lastname}</td>
                <td>${user.email}</td>
                <td>${user.phone}</td>
                <td>${user.active_machine}</td>
                <td>${user.balance}</td>
                <td>
                    <div class="btn-group-vertical" role="group" aria-label="Basic example">
                        <div class="btn-group">
                            <button type="button" class="btn btn-info dropdown-toggle" data-bs-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item btn-success" href="?id=${user.id}">Deactivate</a>
                            <a class="dropdown-item" href="php/admin.php/deleteuser?id=${user.id}">Delete</a>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        `;
    }

    placeholder.innerHTML = out;
})
