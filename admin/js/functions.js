function showModal() {
    const modal = document.getElementById("loginModal");
    const modalbg =document.getElementById("modalbg");
    modalbg.style.display = "flex";
    modal.style.display = "flex";
}

function setCookie(name, value, days) {
    const expires = new Date();
    expires.setTime(expires.getTime() + (days * 24 * 60 * 60 * 1000));
    document.cookie = `${name}=${value}; expires=${expires.toUTCString()}; path=/`;
}

function getCookie(name) {
    const match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) return match[2];
}

function validateLogin() {
    const phoneInput = document.querySelector('input[name="phone"]');
    const passwordInput = document.querySelector('input[name="password"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const phone = phoneInput.value;
    const password = passwordInput.value;

    if (!phone || !password) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const loginData = new URLSearchParams();
    loginData.append('phone', phone);
    loginData.append('password', password);

    axios.post('php/login.php', loginData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        if (response.data === 'Login successful') {
            window.location.href = 'pages/dashboard.php'; // Redirect to the dashboard page
        } else if (response.data === 'Admin logged in') {
            window.location.href = 'admin/dashboard.php';
        } else {
            errorMessageDiv.textContent = response.data;
        }
    })
    .catch(error => {
        console.error('Error:', error);
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}

function store() {

    const nameInput = document.querySelector('input[name="name"]');
    const priceInput = document.querySelector('input[name="price"]');
    const incomeInput = document.querySelector('input[name="income"]');
    const imgInput = document.querySelector('input[name="img[]"]');
    const errorMessageDiv = document.getElementById('errorMessage');
    console.log('Clicked');

    const name = nameInput.value;
    const price = priceInput.value;
    const income = incomeInput.value;
    const image = imgInput.value;

    if (!name || !price || !income) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const storeData = new URLSearchParams();
    storeData.append('machine_name', name);
    storeData.append('price', price);
    storeData.append('income', income);
    storeData.append('image', image);

    axios.post('../php/admin.php/add_machine', storeData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        console.log(response.data)
        if (response.data.message === 'Successful') {
            // console.log('success')
            window.location.href = '../pages/withdraw.php?success=true';
        } else {
            errorMessageDiv.textContent = response.data.message;
        }
    })
    .catch(error => {
        console.error('AJAX Error:', error);
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}

function validateSignup() {
    const phoneInput = document.querySelector('input[name="phone"]');
    const passwordInput = document.querySelector('input[name="password"]');
    const password2Input = document.querySelector('input[name="password2"]');
    const firstInput = document.querySelector('input[name="firstname"]');
    const lastInput = document.querySelector('input[name="lastname"]');
    const emailInput = document.querySelector('input[name="email"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const phone = phoneInput.value;
    const password = passwordInput.value;
    const password2 = password2Input.value;
    const email = emailInput.value;
    const firstname = firstInput.value;
    const lastname = lastInput.value;

    if (!phone || !password || !password2 || !email || !firstname || !lastname) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const signupData = new URLSearchParams();
    signupData.append('firstname', firstname);
    signupData.append('lastname', lastname);
    signupData.append('email', email);
    signupData.append('phone', phone);
    signupData.append('password', password);
    signupData.append('password2', password2);

    axios.post('../php/users.php/signup', signupData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        if (response.data.message === 'Registration was successful') {
            window.location.href = '../index.html?success=true';
        } else {
            errorMessageDiv.textContent = response.data.message;
        }
    })
    .catch(error => {
        console.error('AJAX Error:', error);
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}

function withdraw() {
    const amountInput = document.querySelector('input[name="amount"]');
    const passkeyInput = document.querySelector('input[name="passkey"]');
    const user_idInput = document.querySelector('input[name="id"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const amount = amountInput.value;
    const passkey = passkeyInput.value;
    const id = user_idInput.value;

    if (!amount || !passkey) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const withdrawData = new URLSearchParams();
    withdrawData.append('id', id);
    withdrawData.append('amount', amount);
    withdrawData.append('passkey', passkey);

    axios.post('../php/withdraw.php', withdrawData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        // console.log(response.data)
        if (response.data.message === 'Withdrawer request was successful') {
            // console.log('success')
            window.location.href = '../pages/withdraw.php?success=true';
        } else {
            errorMessageDiv.textContent = response.data.message;
        }
    })
    .catch(error => {
        console.error('AJAX Error:', error);
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}

function funduser() {
    const amountInput = document.querySelector('input[name="amount"]');
    const emailInput = document.querySelector('input[name="email"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const amount = amountInput.value;
    const email = emailInput.value;

    if (!amount || !email) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const withdrawData = new URLSearchParams();
    withdrawData.append('amount', amount);
    withdrawData.append('email', email);

    axios.post('php/admin.php/funduser', withdrawData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        // console.log(response.data)
        if (response.data.message === 'Successful') {
            console.log('response.data')
            window.location.href = 'funduser.php?success=true';
        } else {
            errorMessageDiv.textContent = response.data.message;
        }
    })
    .catch(error => {
        if (error.response) {
            console.log('Response data:', error.response.data);
            console.log('Response status:', error.response.status);
        } else {
            console.error('Error:', error.message);
            errorMessageDiv.textContent = 'An error occurred. Please try again later.';
        }
        
    });
}

function debituser() {
    const amountInput = document.querySelector('input[name="amount"]');
    const emailInput = document.querySelector('input[name="email"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const amount = amountInput.value;
    const email = emailInput.value;

    if (!amount || !email) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const withdrawData = new URLSearchParams();
    withdrawData.append('amount', amount);
    withdrawData.append('email', email);

    axios.post('php/admin.php/debituser', withdrawData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        // console.log(response.data)
        if (response.data.message === 'Successful') {
            // console.log('success')
            window.location.href = 'debituser.php?success=true';
        } else {
            errorMessageDiv.textContent = response.data.message;
        }
    })
    .catch(error => {
        console.error('AJAX Error:', error);
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}

function requestfee() {
    const amountInput = document.querySelector('input[name="amount"]');
    const emailInput = document.querySelector('input[name="email"]');
    const errorMessageDiv = document.getElementById('errorMessage');

    const amount = amountInput.value;
    const email = emailInput.value;

    if (!amount || !email) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const withdrawData = new URLSearchParams();
    withdrawData.append('amount', amount);
    withdrawData.append('email', email);

    axios.post('php/admin.php/requestfee', withdrawData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        // console.log(response.data)
        if (response.data.message === 'Successful') {
            window.location.href = 'request.php?success=true';
        } else {
            errorMessageDiv.textContent = response.data.message;
        }
    })
    .catch(error => {
        console.error('AJAX Error:', error);
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}

function passkey() {
    const bankInput = document.getElementById('bank');
    const account_nameInput = document.querySelector('input[name="account_name"]');
    const account_numberInput = document.querySelector('input[name="account_number"]');
    const passkey1Input = document.querySelector('input[name="passkey1"]');
    const passkey2Input = document.querySelector('input[name="passkey2"]');
    const user_idInput = document.querySelector('input[name="id"]');
    const errorMessageDiv = document.getElementById('errorMessage');
    // console.log('Clicked');

    const bank = bankInput.value;
    const account_name = account_nameInput.value;
    const account_number = account_numberInput.value;
    const passkey1 = passkey1Input.value;
    const passkey2 = passkey2Input.value;
    const id = user_idInput.value;

    // console.log ('bank', bank);
    // console.log ('name', account_name);
    // console.log ('number', account_number);
    // console.log ('id', id);

    if (!bank || !passkey1 || !account_name || !account_number || !passkey2) {
        errorMessageDiv.textContent = 'Please fill in all required fields.';
        return;
    }

    const passkeyData = new URLSearchParams();
    passkeyData.append('id', id);
    passkeyData.append('bank', bank);
    passkeyData.append('account_name', account_name);
    passkeyData.append('account_number', account_number);
    passkeyData.append('passkey1', passkey1);
    passkeyData.append('passkey2', passkey2);

    axios.post('../php/users.php/passkey', passkeyData.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then(response => {
        console.log(response.data)
        if (response.data.message === 'Successful') {
            window.location.href = '../pages/passkey.html?success=true';
        } else {
            errorMessageDiv.textContent = response.data.message;
        }
    })
    .catch(error => {
        console.error('AJAX Error:', error);
        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
    });
}