ErrorDocument 404 404.html

<IfModule mod_rewrite.c>

    Options -Indexes -MultiViews +FollowSymLinks

    # Header always add Access-Control-Allow-Methods "POST, GET, PUT, OPTIONS, PATCH, DELETE"
    # Header always add Access-Control-Allow-Headers "X-Accept-Charset,X-Accept,Content-Type"

    RewriteEngine ON

    RewriteCond %{REQUEST_METHOD} OPTIONS
    RewriteRule ^(.*)$ $1 [R=200,L,E=HTTP_ORIGIN:%{HTTP:ORIGIN}]

    RewriteCond %{HTTP:Authorization} ^(.+)$
    RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
    RewriteBase /
    

    # Handle Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f

    # RewriteRule ^(.+)$ index.php?action=$1 [L,QSA]
    # RewriteRule ^index/([0-9]+)/([0-9a-zA-Z_-]+)$ index.php?token=$1&title=$2 [NC,L] 
    RewriteRule ^([0-9a-zA-Z_-]+)(.+)$ index.php?endpoint=$1&params=$2 [QSA] 
    RewriteRule ^([0-9a-zA-Z_-]+)(.+)/$ index.php?endpoint=$1&params=$2 [QSA]  
</IfModule>