<?php
require_once 'config.php';
use PHPMailer\PHPMailer\PHPMailer;
require 'PHPMailer-master/PHPMailer-master/src/PHPMailer.php';
require 'PHPMailer-master/PHPMailer-master/src/SMTP.php';
require 'PHPMailer-master/PHPMailer-master/src/Exception.php';
class projectAPI{
    private $sqlConn;
    private $users;
    private $myAsset;
    private $t_Table;
    private $store;
    private $d_board;
    private $message;

    public function __construct($usersTable, $usersAsset, $tTable, $storeTable, $dashboard, $mes, $conn){
        $this->sqlConn = $conn;
        $this->users = $usersTable;
        $this->myAsset = $usersAsset;            
        $this->t_Table = $tTable;
        $this->store = $storeTable;
        $this->d_board = $dashboard;  
        $this->message = $mes;       
    }
    public function sendMail($subject, $body, $email) {
        $mail = new PHPMailer(true);
        // $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = 'server343.web-hosting.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@isbrkonline.com';
        $mail->Password = 'Dino-boy123';
        $mail->Port = 465; 
        $mail->SMTPSecure = 'ssl';
        
        $mail->setFrom('noreply@isbrkonline.com', 'Isbrkonline'); 
        $mail->addAddress($email); 
        
        $mail->Subject = $subject;
        $mail->Body = $body;
        
        if ($mail->send()) {
            return true;
        } 
        else {
            return false;
        }
    }
    public function sendHelp($subject, $body, $email) {
        $mail = new PHPMailer(true);
        // $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = 'server343.web-hosting.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@isbrkonline.com';
        $mail->Password = 'Dino-boy123';
        $mail->Port = 465; 
        $mail->SMTPSecure = 'ssl';
        
        $mail->setFrom('noreply@isbrkonline.com', 'Message'); 
        $mail->addAddress($email); 
        
        $mail->Subject = $subject;
        $mail->Body = $body;
        
        if ($mail->send()) {
            return true;
        } 
        else {
            return false;
        }
    }
    public function randomToken($length = 16) {
        $characters = '1234567890';
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $token;
    }

    //Automation
    public function priceDrop() {
        $iDs = $this->fetchAllAssetsBought();
        $successCount = 0;
        $failureCount = 0;
    
        foreach ($iDs as $machine_id) {
            $stmt1 = $this->sqlConn->prepare("SELECT * FROM $this->myAsset WHERE id = ?");
            $stmt1->bind_param("i", $machine_id);
            $stmt1->execute();
            $result1 = $stmt1->get_result();
            
            if ($result1 && $result1->num_rows > 0) {
                $row1 = $result1->fetch_assoc();
                $ini_price = $row1['ini_price'];
                $percentage = $ini_price / 65; //drop the price
                $price = $row1['price'];
                $user_id = $row1['user_id'];
                if ($percentage <= $price) {
                    $calculatedValue = $price - $percentage;
                    $calculatedValue = round($price - $percentage, 2);
                    $stmt3 = $this->sqlConn->prepare("UPDATE $this->myAsset SET price = ? WHERE id = ?");
                    $stmt3->bind_param("di", $calculatedValue, $machine_id);
                    if ($stmt3->execute()) {
                        $successCount++;
                    } else {
                        $failureCount++;
                    }
                } else {
                    //remove the machine
                    $stmt4 = $this->sqlConn->prepare("DELETE FROM $this->myAsset WHERE id = ?");
                    $stmt4->bind_param("i", $machine_id);
                    if ($stmt4->execute()) {
                        $stmt5 = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE id = ?");
                        $stmt5->bind_param("i", $user_id);
                        if ($stmt5->execute()) {
                            $result5 = $stmt5->get_result();
                            $row5 = $result5->fetch_assoc();
                            $email = $row5['email'];
                            $active_machine = $row5['active_machine'];
                            $user_id = $row5['id'];
                            $new_active = $active_machine - 1;
                            $stmt3 = $this->sqlConn->prepare("UPDATE $this->users SET active_machine = ? WHERE id = ?");
                            $stmt3->bind_param("ii", $new_active, $user_id);
                            if ($stmt3->execute()) { 
                                $successCount++;
                            } else {
                                $failureCount++;
                            }
                        } else {
                            $failureCount++;
                        }
                        
                    } else {
                        $failureCount++;
                    }
                }
                
            }
        }
    
        return [
            'success' => $successCount,
            'failure' => $failureCount
        ];
    }

    public function fetchFirstMessage() {
        $stmt = $this->sqlConn->prepare("SELECT id FROM $this->message LIMIT 1");
        $stmt->execute();
        $result = $stmt->get_result();
    
        $firstRow = $result->fetch_assoc();
        if ($firstRow) {
            return $firstRow['id'];
        }
    
        return null; // Return null if no rows are found
    }
    

    public function forwardMessage() {
        $id = $this->fetchFirstMessage();
        $stmt1 = $this->sqlConn->prepare("SELECT * FROM $this->message WHERE id = ?");
        $stmt1->bind_param("i", $id);
        $stmt1->execute();
        $result1 = $stmt1->get_result();
        
        if ($result1 && $result1->num_rows > 0) {
            $row1 = $result1->fetch_assoc();
            $subject = 'Welcome to IsBank';
            $user_email = $row1['email'];
            $username = $row1['username'];
            $email = $user_email;

            $stmt2 = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE email = ?");
            $stmt2->bind_param("s", $user_email);
            $stmt2->execute();
            $result2 = $stmt2->get_result();
            
            if ($result2 && $result2->num_rows > 0) {
                $row2 = $result2->fetch_assoc();
                $account_number = $row2['account_number'];
                $body = "Dear $username ,
\n
\n
Welcome to IsBank! We're excited to have you on board. Your new account is now active, and we're here to make your banking experience smooth and convenient.
\n
Account Number: $account_number
                
With IsBank, you can:
                
Access Online Banking: Manage your finances anytime, anywhere.
Track Transactions: Keep an eye on your balance and recent transactions.
Pay Bills: Easily pay your bills online.
Secure Your Account: We prioritize your account security.
To start exploring your account, visit http://isbrkonline.com and log in using your account number and the password you created during signup.
                
Should you need assistance or have any questions, please reach out to our support team.
                
Thank you for choosing IsBank. We're here to support your financial journey!
Warm regards.";
                $sendmail = $this->sendHelp($subject, $body, $email);
                if ($sendmail) {
                    $stmt4 = $this->sqlConn->prepare("DELETE FROM $this->message WHERE id = ?");
                    $stmt4->bind_param("i", $id);
                    if ($stmt4->execute()) {
                        return true;
                    }
                }
            }
        }
    }

    public function autoReply() {
        $id = $this->fetchFirstMessage();
        $stmt1 = $this->sqlConn->prepare("SELECT * FROM $this->message WHERE id = ?");
        $stmt1->bind_param("i", $id);
        $stmt1->execute();
        $result1 = $stmt1->get_result();
        
        if ($result1 && $result1->num_rows > 0) {
            $row1 = $result1->fetch_assoc();
            $subject = $row1['subject'];
            $user_email = $row1['email'];
            $email = $user_email;

            $stmt2 = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE email = ?");
            $stmt2->bind_param("s", $user_email);
            $stmt2->execute();
            $result2 = $stmt2->get_result();
            
            if ($result2 && $result2->num_rows > 0) {
                $row2 = $result2->fetch_assoc();
                $firstname = $row2['firstname'];
                $body = "Hello " . $firstname . "," .
                "\n" .
                "\n" .
                "Thank you for contacting Isbrkonline!" .
                "\n" .
                "We’re receiving an unusually high volume of requests at this time, and this has impacted our response and resolution timelines. We will, however, aim to respond to all inquiries within 24 hours of receiving the request. We know that you have trusted us, and we apologize for this delay." .
                "\n" .
                "\n" .
                "Please know that we have received your message, and you do not need to send it multiple times as this will likely delay the responses. We are committed to resolving your issue as soon as possible." .
                "\n" .
                "\n" .
                "Please note that Isbrkonline will never ask for your Account Password, Passkey, Credit card details, etc. For your Privacy and Security, NEVER send or share sensitive information or credentials via email, phone call, chat, or social media." .
                "\n" .
                "\n" .
                "\n" .
                "Thank you for your patience and understanding." .
                "\n" .
                "\n" .
                "\n" .
                "Regards," . 
                "\n" .
                "Isbrk Team!";
                $sendmail = $this->sendHelp($subject, $body, $email);
                if ($sendmail) {
                    $stmt4 = $this->sqlConn->prepare("DELETE FROM $this->message WHERE id = ?");
                    $stmt4->bind_param("i", $id);
                    if ($stmt4->execute()) {
                        return true;
                    }
                }
            }
        }
    }

    public function fetchAllAssetsBought() {
        $stmt = $this->sqlConn->prepare("SELECT id FROM $this->myAsset");
        $stmt->execute();
        $result = $stmt->get_result();

        $iDs = [];
        while ($row = $result->fetch_assoc()) {
            $iDs[] = $row['id'];
        }

        return $iDs;
    }

    public function fetchAllUserIDs() {
        $stmt = $this->sqlConn->prepare("SELECT DISTINCT user_id FROM $this->myAsset");
        $stmt->execute();
        $result = $stmt->get_result();

        $userIDs = [];
        while ($row = $result->fetch_assoc()) {
            $userIDs[] = $row['user_id'];
        }

        return $userIDs;
    }
    //Automation
    public function processUserIncome() {
        $userIDs = $this->fetchAllUserIDs();
        $successCount = 0;
        $failureCount = 0;
    
        foreach ($userIDs as $userID) {
            $stmt1 = $this->sqlConn->prepare("SELECT SUM(profit) as total_profit FROM $this->myAsset WHERE user_id = ?");
            $stmt1->bind_param("i", $userID);
            $stmt1->execute();
            $result1 = $stmt1->get_result();
            
            if ($result1 && $result1->num_rows > 0) {
                $row1 = $result1->fetch_assoc();
                $total_profit = $row1['total_profit'];

                $stmt22 = $this->sqlConn->prepare("SELECT * FROM $this->myAsset WHERE user_id = ?");
                $stmt22->bind_param("i", $userID);
                $stmt22->execute();
                $result22 = $stmt22->get_result();
                $row22 = $result22->fetch_assoc();
                $machine_name = $row22['machine_name'];
    
                $stmt2 = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE id = ?");
                $stmt2->bind_param("i", $userID);
                $stmt2->execute();
                $result2 = $stmt2->get_result();
                
                if ($result2 && $result2->num_rows > 0) {
                    $row2 = $result2->fetch_assoc();
                    $user_balance = $row2['balance'];
                    $email = $row2['email'];
                    $firstname = $row2['firstname'];
    
                    $new_balance = $user_balance + $total_profit;
                    
                    $stmt3 = $this->sqlConn->prepare("UPDATE $this->users SET balance = ? WHERE id = ?");
                    $stmt3->bind_param("di", $new_balance, $userID);
                    if ($stmt3->execute()) {
                        $role = 'user';
                        $description = 'Daily profit +';
                        $status = 1;
                        $date = $this->time();
                        $transaction_id = $this->randomToken();
                        $stmt4 = $this->sqlConn->prepare("INSERT INTO $this->t_Table (user_id, amount, role, description, status, ref_no, date) VALUES (?, ?, ?, ?, ?, ?, ?)");
                        $stmt4->bind_param("sssssss", $userID, $total_profit, $role, $description, $status, $transaction_id, $date);
                        if ($stmt4->execute()) {
                            $subject = 'Credit Alert';
                            $body = 'Dear ' . $firstname . ', ' .
                            "\n" .
                            "\n" .
                            "Your account has been credited with ₦" . 
                            $total_profit . 
                            " from your hashpower miner." . 
                            "\n" .
                            "\n" .
                            "To make even more profit in the future, you might want to think about getting more rigs from our assets store." . 
                            "\n" .
                            "\n" .
                            
                            "Thanks for being part of our mining community. We're here to support your success." .
                            "\n" .
                            "\n" .
                            "Best regards" .
                            "\n" .
                            "Isbrkonline";
                            $sendmail = $this->sendMail($subject, $body, $email);
                            if ($sendmail) {
                                $successCount++;
                            }
                            
                        } else {
                            $failureCount++;
                        }
                    } 
                }
            }
        }
    
        return [
            'success' => $successCount,
            'failure' => $failureCount
        ];
    }

    public function myAssets($user_id) {
        $stmt = $this->sqlConn->prepare("SELECT * FROM $this->myAsset WHERE user_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result) {
            $row = $result->fetch_all(MYSQLI_ASSOC);
            return $row;
        } else {
            return [
                'message' => 'No assets yet. Buy from our store',
            ];
        }
    }

    public function time() {
        $timezone = new DateTimeZone('GMT');
        $currentDateTime = new DateTime('now', $timezone);
        $currentDateTime->modify('+1 hour');

        $formattedDateTime = $currentDateTime->format('Y-m-d H:i:s');

        return $formattedDateTime;
    }

    public function handleRequest($method, $endpoint, $data){
        switch ($endpoint) {
            case '/incomeadd':
                if ($method === 'GET') {
                    $run = $this->processUserIncome();
                    return $run;
                } else {
                    return [
                        'message' => 'Invalid request method.',
                    ];
                }
                break;
            case "/drop":
                if ($method === 'GET') {
                    $response = $this->priceDrop();
                    return $response;
                } else {
                    return [
                        'message' => 'Invalid request method.',
                    ];
                }
                break;
            case "/welcomemessage":
                if ($method === 'GET') {
                    $response = $this->forwardMessage();
                    if ($response) {
                        return [
                            'message' => 'Sent',
                        ];
                    }
                } else {
                    return [
                        'message' => 'Invalid request method.',
                    ];
                }
                break;
                

            default:
                return 'Invalid endpoint';
        }
    }
    
}

$usersTable = 'users';
$usersAsset = 'usersasset';
$tTable = 'transactions';
$storeTable = 'store';
$dashboard = 'dashboard';
$mes = 'new_user_queue';


$api = new projectAPI($usersTable, $usersAsset, $tTable, $storeTable, $dashboard, $mes, $conn);

$method = $_SERVER['REQUEST_METHOD'];
$endpoint = parse_url($_SERVER['PATH_INFO'], PHP_URL_PATH);
$endpoint = rtrim($endpoint, '/');



$data = $_POST; 
$response = $api->handleRequest($method, $endpoint, $data);
header('Content-Type: application/json');
echo json_encode($response);

