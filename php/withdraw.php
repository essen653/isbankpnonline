<?php
    session_start();
    require_once "config.php";

    $response = array();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['amount']) && isset($_POST['passkey']) && isset($_POST['id'])) {
            $amount= $_POST['amount'];
            $passkey1= $_POST['passkey'];
            $id = $_POST['id'];

            $sql = "SELECT * FROM `users` WHERE id = ?";
            $stmt = $conn->prepare($sql); 
            $stmt->bind_param("s", $id);
            $stmt->execute();
            $row = $stmt->get_result()->fetch_assoc();
    
            if ($row) {
                $user_id = $row['id'];
                $passkey2 = $row['passkey'];
                $email = $row['email'];
                $account_balance = $row['balance'];
                $bank_name = $row['bank_name'];
                $account_name = $row['account_name'];
                $account_number = $row['account_number'];
                $firstname = $row['firstname'];
                $w_amount = $row['total_withdrawer'];


                $length = 16;
                $characters = '1234567890';
                $token = '';
                for ($i = 0; $i < $length; $i++) {
                    $token .= $characters[rand(0, strlen($characters) - 1)];
                }

                if ($passkey1 === $passkey2) {
                    if ($amount >= 3450) {
                        if ($amount < $account_balance) {
                            $new_balance = $account_balance - $amount;
                            $ref_no = $token;
                            $n_amount = $w_amount + $amount;
                            $timezone = new DateTimeZone('GMT');
                            $currentDateTime = new DateTime('now', $timezone);
                            $currentDateTime->modify('+1 hour');
                            $formattedDateTime = $currentDateTime->format('Y-m-d H:i:s');
                            $update = "UPDATE `users` SET `balance` = $new_balance, `total_withdrawer` = $n_amount WHERE `id` = '$user_id' ";
        
                            $query = $conn->query($update);
                            if ($query) {
                                $insert1= "INSERT INTO `withdrawer` (`email`, `firstname`, `amount`, `bank_name`, `account_name`, `account_number`, `ref_no`) VALUES ('$email', '$firstname', '$amount', '$bank_name', '$account_name', '$account_number', '$ref_no')";

                                $result1 = $conn->query($insert1);
                                if ($result1) {
                                    $role = 'user';
                                    $description = 'Withdrawer of ';
                                    $insert2= "INSERT INTO `transactions` (`user_id`, `amount`, `description`, `ref_no`, `date`, `role`) VALUES ('$user_id', '$amount', '$description', '$ref_no', '$formattedDateTime', '$role')";
                                    $result2 = $conn->query($insert2);
                                    if ($result2) {
                                      $response['message'] = 'Withdrawer request was successful';
                                    } else {
                                        $response['message'] = 'Failed to update your balance, try again.';
                                    }
                                } else {
                                    $response['message'] = 'Failed to update your balance, try again.';
                                }
                            } else {
                                $response['message'] = 'Failed to update your balance, try again.';
                            }
                        } else {
                            $response['message'] = 'Insufficient funds';
                        }
                    } else {
                        $response['message'] = 'Sorry your withdrawer amount is below ₦3450';
                    }
                    
                } else {
                    $response['message'] = 'Sorry your passkey is incorrect';
                }

            } else {
                $response['message'] = 'Your user id not found';
            }
        }
    } else {
        $response['message'] = 'Please fill all input field';
    }
    header('Content-Type: application/json');
    echo json_encode($response);
?>