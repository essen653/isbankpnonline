$(document).ready(function() {
    var rowsPerP = 5;
    var currentPage = 1;
    function showRows(rowsPerP, currentPage) {
      var rows = $("#myTable tbody tr");
      var startIndex = (currentPage - 1) * rowsPerP;
      var endIndex = startIndex + rowsPerP;
      rows.hide();
      rows.slice(startIndex, endIndex).show();
    }

    function updateDashboard(data) {
            const phoneElement = document.getElementById('phone');
            if (phoneElement) {
                phoneElement.value = data.phone;
                phoneElement.textContent = data.phone;
            }
            const idElement = document.getElementById('id');
            if (idElement) {
                idElement.value = data.id;
                idElement.textContent = data.id;
            }
            const firstnameElement = document.getElementById('firstname');
            if (firstnameElement) {
                firstnameElement.textContent = data.firstname;
                firstnameElement.value = data.firstname;
            }
            const lastnameElement = document.getElementById('fullname');
            if (lastnameElement) {
                lastnameElement.textContent = data.fullname;
                lastnameElement.value = data.fullname;
            }

            const balanceElement = document.getElementById('balance');
            if (balanceElement) {
                balanceElement.textContent = data.balance; 
            }
            const balance2Element = document.getElementById('total_deposit');
            if (balance2Element) {
                balance2Element.textContent = data.total_deposit; 
            }
            const emailElement = document.getElementById('email');
            if (emailElement) {
                emailElement.textContent = data.email;
                emailElement.value = data.email;
            }
            const feesElement = document.getElementById('fees');
            if (feesElement) {
                feesElement.textContent = data.fees;
                feesElement.value = data.fees;
            }
            const a_numberElement = document.getElementById('account_number');
            if (a_numberElement) {
                a_numberElement.textContent = data.account_number;
            }
            
            const transactionElement = document.getElementById('total_transaction');
            if (transactionElement) {
                transactionElement.textContent = data.transaction;
            }
    }

    function initializePagination(rows) {
        var numPages = Math.ceil(rows.length / rowsPerP);
        var pagination = $(".pagination");
        function updatePagination() {
            pagination.empty();
            var prevButton = $("<li class='page-item'><a class='page-link' href='#'><span aria-hidden='true'>&laquo;</span></a></li>");
            pagination.append(prevButton);

            var prevEllipsis = $("<li class='page-item disabled'><span class='page-link'>&hellip;</span></li>");

            for (var i = 1; i <= numPages; i++) {
                if (i === currentPage) {
                    var pageLink = $("<li class='page-item active'><span class='page-link'>" + i + "</span></li>");
                } else if (i <= 2 || i >= numPages - 1 || (i >= currentPage - 1 && i <= currentPage + 1)) {
                    var pageLink = $("<li class='page-item'><a class='page-link' href='#'>" + i + "</a></li>");
                } else {
                    if (!prevEllipsis.hasClass('disabled')) {
                        pagination.append(prevEllipsis);
                        prevEllipsis = $("<li class='page-item disabled'><span class='page-link'>&hellip;</span></li>");
                    }
                    continue;
                }

                if (i <= 2 || i >= numPages - 1 || (i >= currentPage - 1 && i <= currentPage + 1)) {
                    pagination.append(pageLink);
                } else {
                    prevEllipsis = $("<li class='page-item disabled'><span class='page-link'>&hellip;</span></li>");
                }
            }
            var nextButton = $("<li class='page-item'><a class='page-link' href='#'><span aria-hidden='true'>&raquo;</span></a></li>");
            pagination.append(nextButton);

            showRows(rowsPerP, currentPage);
        }

        updatePagination();

        pagination.on("click", ".page-link", function (e) {
            e.preventDefault();
            var clickedPage = $(this).text();

            if (clickedPage === "«") {
                currentPage = Math.max(currentPage - 1, 1);
            } else if (clickedPage === "»") {
                currentPage = Math.min(currentPage + 1, numPages);
            } else {
                currentPage = parseInt(clickedPage);
            }
            updatePagination();
        });
    }
  
    function fetchUserDetailsAndTransactions() {
      fetch('php/session_info.php')
        .then(response => response.json())
        .then(data => {
          updateDashboard(data);
          fetchTransactionDetails(data.id, initializePagination);
          fetchDebitTransaction(data.id, initializePagination);
        })
        .catch(error => console.error('Error:', error))
        .finally(() => {
          setTimeout(fetchUserDetailsAndTransactions, 5000);
        });
    }
  
    function fetchTransactionDetails(userId, callback) {
      fetch(`php/users.php/transactions?user_id=${userId}`)
        .then(response => response.json())
        .then(transactions => {
          const tableBody = document.querySelector('#myTable tbody');
          if (transactions.length > 0) {
            
                if (tableBody) {
                    tableBody.innerHTML = ''; 
    
                    transactions.reverse();
    
                    transactions.forEach(transaction => {
                        if (transaction.description === "Deposit of "){
                            const row = document.createElement('tr');
        
                            const amountCell = document.createElement('td');
                            const des = document.createElement('span');
                            des.textContent = transaction.description + ' ';
                            const amountText = document.createElement('strong');
                            amountText.textContent = ' $'+transaction.amount;
                            amountCell.appendChild(des);
                            amountCell.appendChild(amountText);
        
                            const dateCell = document.createElement('td');
                            const date = document.createElement('span');
                            dateCell.className = 'smalldate';
                            dateCell.textContent = transaction.date;
        
                            dateCell.appendChild(date);
        
                            const satusCell = document.createElement('td');
                            const statusBadge = document.createElement('div');
        
                            const pending = 'Pending';
                            const approved = 'Successful';
                            const declined = 'Declined';
        
                            const status = transaction.status;
        
                            if (status == 0) {
                                statusBadge.className = 'badge badge-opacity-warning';
                                statusBadge.textContent = pending;
                                satusCell.appendChild(statusBadge);
                            } else if (status == 1) {
                                statusBadge.className = 'badge badge-opacity-success';
                                statusBadge.textContent = approved;
                                satusCell.appendChild(statusBadge);
                            } else {
                                statusBadge.className = 'badge badge-opacity-danger';
                                statusBadge.textContent = declined;
                                satusCell.appendChild(statusBadge);
                            }
                            satusCell.appendChild(statusBadge);
        
                            row.appendChild(amountCell);
                            row.appendChild(dateCell);
                            row.appendChild(satusCell);
        
                            tableBody.appendChild(row);
                        }
                    });
                }
            
            } else {
                tableBody.innerHTML = '<tr><td colspan="2">No transactions available.</td></tr>';
            }
  
          if (callback) {
            callback(tableBody.children);
          }
        })
        .catch(error => console.error('Error:', error));
    }

    function fetchDebitTransaction(userId, callback) {
        fetch(`php/users.php/transactions?user_id=${userId}`)
          .then(response => response.json())
          .then(transactions => {
            const tableBody = document.querySelector('#myTable2 tbody');
            if (transactions.length > 0) {
              
                  if (tableBody) {
                      tableBody.innerHTML = ''; 
      
                      transactions.reverse();
      
                      transactions.forEach(transaction => {
                          if (transaction.description === "Debit of "){
                              const row = document.createElement('tr');
          
                              const amountCell = document.createElement('td');
                              const des = document.createElement('span');
                              des.textContent = transaction.description + ' ';
                              const amountText = document.createElement('strong');
                              amountText.textContent = ' $'+transaction.amount;
                              amountCell.appendChild(des);
                              amountCell.appendChild(amountText);
          
                              const dateCell = document.createElement('td');
                              const date = document.createElement('span');
                              dateCell.className = 'smalldate';
                              dateCell.textContent = transaction.date;
          
                              dateCell.appendChild(date);
          
                              const satusCell = document.createElement('td');
                              const statusBadge = document.createElement('div');
          
                              const pending = 'Pending';
                              const approved = 'Successful';
                              const declined = 'Declined';
          
                              const status = transaction.status;
          
                              if (status == 0) {
                                  statusBadge.className = 'badge badge-opacity-warning';
                                  statusBadge.textContent = pending;
                                  satusCell.appendChild(statusBadge);
                              } else if (status == 1) {
                                  statusBadge.className = 'badge badge-opacity-success';
                                  statusBadge.textContent = approved;
                                  satusCell.appendChild(statusBadge);
                              } else {
                                  statusBadge.className = 'badge badge-opacity-danger';
                                  statusBadge.textContent = declined;
                                  satusCell.appendChild(statusBadge);
                              }
                              satusCell.appendChild(statusBadge);
          
                              row.appendChild(amountCell);
                              row.appendChild(dateCell);
                              row.appendChild(satusCell);
          
                              tableBody.appendChild(row);
                          }
                      });
                  }
              
              } else {
                  tableBody.innerHTML = '<tr><td colspan="2">No transactions available.</td></tr>';
              }
    
            if (callback) {
              callback(tableBody.children);
            }
          })
          .catch(error => console.error('Error:', error));
    }
     
    fetchUserDetailsAndTransactions();
  });
  