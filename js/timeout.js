// Set the session timeout in milliseconds (1 hour)
const sessionTimeout = 60 * 60 * 100;

// Variable to store the timestamp of the last user activity
let lastActivityTimestamp = Date.now();

// Function to check user activity and log out if inactive
function checkUserActivity() {
    const currentTime = Date.now();
    const elapsed = currentTime - lastActivityTimestamp;

    if (elapsed >= sessionTimeout) {
        // Perform logout or redirect to a logout page
        // For example, you can redirect to a logout script
        window.location.href = '../php/logout.php';
    } else {
        // Continue checking user activity
        setTimeout(checkUserActivity, 60000); // Check every minute
    }
}

// Event listener for user activity
document.addEventListener('mousemove', () => {
    lastActivityTimestamp = Date.now();
});

// Start checking user activity
checkUserActivity();
