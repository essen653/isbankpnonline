function fetchUserID() {
    return fetch('../php/session_info.php')
        .then(response => response.json())
        .then(data => {
            // console.log(data.id)
            return data.id; 
        })
        .catch(error => {
            console.error('Error:', error);
            return null;
        });
}

async function fetchMachineDetails() {
    try {
        const response = await fetch('../php/users.php/getMachineDetails'); 

        if (!response.ok) {
            throw new Error(`Fetch error: ${response.status} - ${response.statusText}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Error fetching machine details:', error);
        return [];
    }
}

async function populateMachineDetails() {
    // console.log(fetchUserID());
    const machineDetails = await fetchMachineDetails();
    const machineContainer = document.getElementById('machineContainer');

    machineDetails.forEach(machine => {
        const boxCard = document.createElement('div');
        boxCard.className = 'box-card box1';

        const storeInnerFlex = document.createElement('div');
        storeInnerFlex.className = 'store-innerflex';

        const flexColumn1 = document.createElement('div');
        flexColumn1.className = 'flex-column';
        flexColumn1.style.padding = '20px';

        const img = document.createElement('img');
        img.className = 'box-img';
        img.src = `../images/dashboard/${machine.img}`;
        img.alt = 'plus';

        const button = document.createElement('button');
        button.style.border = '0px';
        button.className = 'machine-button bg-success';
        button.innerText = 'Buy Asset';
        button.setAttribute('data-machine-id', machine.id);

        button.addEventListener('click', () => {
            const modal = document.getElementById('confirmationModal');
            modal.style.display = 'flex';

            const confirmButton = document.getElementById('confirmButton');
            const cancelButton = document.getElementById('cancelButton');

            confirmButton.addEventListener('click', async () => {
                modal.style.display = 'none';

                const machineId = machine.id;
                const userId = await fetchUserID();

                const url = `../php/users.php/buy?id=${userId}&machine_id=${machineId}`;

                try {
                    const response = await fetch(url);
                    const data = await response.json();

                    if (data && data.message) {
                        const errorMessageElement = document.getElementById('error');
                        errorMessageElement.textContent = data.message;
                        errorMessageElement.style.display = "flex";

                        if (data.message === 'Successful') {
                            const successElement = document.getElementById('success');
                            successElement.textContent = "Congratulations, your machine has been added to your asset.";
                            successElement.style.display = "flex";
                            errorMessageElement.style.display = "none";
                        }
                    }
                } catch (error) {
                    console.error('Error:', error);
                }
            });

            cancelButton.addEventListener('click', () => {
                modal.style.display = 'none';
            });
        });


        flexColumn1.appendChild(img);
        flexColumn1.appendChild(button);

        const flexColumn2 = document.createElement('div');
        flexColumn2.className = 'flex-column';

        const machineName = document.createElement('span');
        machineName.className = 'machine-name';
        machineName.innerText = machine.machine_name;

        const price = document.createElement('div');
        price.style.color = 'green';
        price.innerHTML = `<strong>₦${machine.price}</strong><i class="mdi mdi-arrow-up-bold small"></i>`;

        const revenue = document.createElement('span');
        revenue.style.fontSize = '13px';
        revenue.innerHTML = `<span>Daily income:</span><strong>₦${machine.income}</strong>`;

        flexColumn2.appendChild(machineName);
        flexColumn2.appendChild(price);
        flexColumn2.appendChild(revenue);


        storeInnerFlex.appendChild(flexColumn1);
        storeInnerFlex.appendChild(flexColumn2);

        boxCard.appendChild(storeInnerFlex);
        machineContainer.appendChild(boxCard);
    });
}

window.onload = populateMachineDetails;