function share() {
    const modaL = document.getElementById('confirmationModal');
    modaL.style.display = 'flex';
    const cancelButton = document.getElementById('cancelButton');
    const whatsapp = document.getElementById('whatsapp');
    const facebook = document.getElementById('facebook');

    const ref_id = document.getElementById('ref_id');
    const id = ref_id.value;
    
    const copy = document.getElementById('copy');

    cancelButton.addEventListener('click', async () => {
        modaL.style.display = 'none';
    });

    var message = "Hi, Join me on DxMiners hashpower provider! It’s super simple - Your mining rigs are already set up and running. Register with my user ID: " + id + " ";
    var url = "https://app.dxminers.com/signup";
    var shareMessage = message + "\n\n" + url;

    facebook.addEventListener('click', async () => {
        window.open("https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(url));
    });

    whatsapp.addEventListener('click', async () => {
        window.open("https://api.whatsapp.com/send?text=" + encodeURIComponent(shareMessage));
    });

    copy.addEventListener('click', async () => {
         modaL.style.display = 'none';
        // var url = "https://app.dxminers.com/signup";

        // Create a temporary input element
        var tempInput = document.createElement("input");
        tempInput.value = shareMessage;
        document.body.appendChild(tempInput);

        // Select the text in the input element
        tempInput.select();
        tempInput.setSelectionRange(0, 99999); // For mobile devices

        // Copy the text to the clipboard
        document.execCommand("copy");

        // Remove the temporary input element
        document.body.removeChild(tempInput);

        alert("Copied!");
    });

}