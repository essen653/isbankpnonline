// $(document).ready(function() {
//   var rowsPerP = 5;
  
//   function showRows(rowsPerP, currentPage) {
//     var rows = $("#myTable tbody tr");
//     var startIndex = (currentPage - 1) * rowsPerP;
//     var endIndex = startIndex + rowsPerP;

//     rows.hide();
//     rows.slice(startIndex, endIndex).show();
//     setTimeout(showRows, 5000);
//   }

//   function initializePagination() {
//     var rows = $("#myTable tbody tr");
//     var numPages = Math.ceil(rows.length / rowsPerP);
//     var currentPage = 1;
//     var pagination = $(".pagination");

//     function updatePagination() {
//       pagination.empty();

//       // Create previous button
//       var prevButton = $("<li class='page-item'><a class='page-link' href='#'><span aria-hidden='true'>&laquo;</span></a></li>");
//       pagination.append(prevButton);

//       for (var i = 1; i <= numPages; i++) {
//         var pageLink = $("<li class='page-item'><a class='page-link' href='#'>" + i + "</a></li>");
//         if (i === currentPage) {
//           pageLink.addClass("active");
//         }
//         pagination.append(pageLink);
//       }

//       // Create next button
//       var nextButton = $("<li class='page-item'><a class='page-link' href='#'><span aria-hidden='true'>&raquo;</span></a></li>");
//       pagination.append(nextButton);

//       showRows(rowsPerP, currentPage);
//     }

//     updatePagination();
    

//     pagination.on("click", ".page-link", function (e) {
//       e.preventDefault();
//       var clickedPage = $(this).text();
      
//       if (clickedPage === "«") {
//         currentPage = Math.max(currentPage - 1, 1);
//       } else if (clickedPage === "»") {
//         currentPage = Math.min(currentPage + 1, numPages);
//       } else {
//         currentPage = parseInt(clickedPage);
//       }

//       updatePagination();
//     });
//   }
  
//   initializePagination();
//   // setTimeout(initializePagination, 50);
// });