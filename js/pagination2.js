
$(document).ready(function() {
    var rowsPerPage = 10; // Number of rows to display per page
  
    // Show the specified number of rows per page
    function showRows2(rowsPerPage, currentPage) {
      var rows = $("#approved tbody tr");
      var startIndex = (currentPage - 1) * rowsPerPage;
      var endIndex = startIndex + rowsPerPage;
  
      rows.hide(); // Hide all rows
      rows.slice(startIndex, endIndex).show(); // Show only the rows for the current page
    } 
  
    function initializePagination2() {
      var rows = $("#approved tbody tr");
      var numPages = Math.ceil(rows.length / rowsPerPage);
      var currentPage = 1; // Add a variable to track the current page
      var pagination = $(".pagination");
    
      pagination.empty(); // Clear any existing pagination links
    
      // Create previous button
      var prevButton = $("<li class='page-item'><a class='page-link' href='#'><span aria-hidden='true'>&laquo;</span></a></li>");
      pagination.append(prevButton);
    
      // Create pagination links
      var startPage = 1;
      var endPage = numPages;
    
      // Adjust the start and end pages based on the current page
      if (currentPage > 2) {
        startPage = currentPage - 1;
      }
      if (currentPage < numPages - 1) {
        endPage = currentPage + 1;
      }
    
      if (startPage > 1) {
        var firstPageLink = $("<li class='page-item'><a class='page-link' href='#'>1</a></li>");
        pagination.append(firstPageLink);
        var ellipsis1 = $("<li class='page-item'><span class='ellipsis'>...</span></li>");
        pagination.append(ellipsis1);
      }
    
      for (var i = startPage; i <= endPage; i++) {
        var pageLink = $("<li class='page-item'><a class='page-link' href='#'>" + i + "</a></li>");
        if (i === currentPage) {
          pageLink.addClass("active");
        }
        pagination.append(pageLink);
      }
    
      if (endPage < numPages) {
        var ellipsis2 = $("<li class='page-item'><span class='ellipsis'>...</span></li>");
        pagination.append(ellipsis2);
        var lastPageLink = $("<li class='page-item'><a class='page-link' href='#'>" + numPages + "</a></li>");
        pagination.append(lastPageLink);
      }
    
      // Create next button
      var nextButton = $("<li class='page-item'><a class='page-link' href='#'><span aria-hidden='true'>&raquo;</span></a></li>");
      pagination.append(nextButton);
    
      // Show the first page by default
      showRows2(rowsPerPage, currentPage);
      pagination.find("li:nth-child(2)").addClass("active"); // Add the 'active' class to the first page
    
      // Handle pagination click events
      pagination.on("click", ".page-link", function (e) {
        e.preventDefault();
    
        // Get the clicked page number or previous/next button
        var clickedPage = $(this).text();
    
        // Determine the new current page based on the clicked page or previous/next button
        if (clickedPage === "«") {
          currentPage = currentPage - 1;
        } else if (clickedPage === "»") {
          currentPage = currentPage + 1;
        } else {
          currentPage = parseInt(clickedPage);
        }
    
        // Validate the new current page and show the corresponding rows
        if (currentPage >= 1 && currentPage <= numPages) {
          showRows2(rowsPerPage, currentPage);
          pagination.find(".active").removeClass("active");
    
          // Update the active page based on the current page
          pagination.find("li:contains(" + currentPage + ")").addClass("active");
    
          // Update the pagination links based on the current page
          pagination.empty(); // Clear existing pagination links
    
          // Create previous button
          pagination.append(prevButton);
    
          // Create pagination linksstartPage = 1;
          endPage = numPages;
    
          // Adjust the start and end pages based on the current page
          if (currentPage > 2) {
            startPage = currentPage - 1;
          }
          if (currentPage < numPages - 1) {
            endPage = currentPage + 1;
          }
    
          if (startPage > 1) {
            var firstPageLink = $("<li class='page-item'><a class='page-link' href='#'>1</a></li>");
            pagination.append(firstPageLink);
            var ellipsis1 = $("<li class='page-item'><span class='ellipsis'>...</span></li>");
            pagination.append(ellipsis1);
          }
    
          for (var i = startPage; i <= endPage; i++) {
            var pageLink = $("<li class='page-item'><a class='page-link' href='#'>" + i + "</a></li>");
            if (i === currentPage) {
              pageLink.addClass("active");
            }
            pagination.append(pageLink);
          }
    
          if (endPage < numPages) {
            var ellipsis2 = $("<li class='page-item'><span class='ellipsis'>...</span></li>");
            pagination.append(ellipsis2);
            var lastPageLink = $("<li class='page-item'><a class='page-link' href='#'>" + numPages + "</a></li>");
            pagination.append(lastPageLink);
          }
    
          // Create next button
          pagination.append(nextButton);
        }
      });
    }
    
    initializePagination2();  
    
  });