(function($) {
  'use strict';
  $(function() {
    $(".nav-settings").on("click", function() {
      $("#right-sidebar").toggleClass("open");
    });
    $(".settings-close").on("click", function() {
      $("#right-sidebar,#theme-settings").removeClass("open");
    });

    $("#settings-trigger").on("click" , function(){
      $("#theme-settings").toggleClass("open");
    });


    //background constants
    var navbar_classes = "navbar-danger navbar-success navbar-warning navbar-dark navbar-light navbar-primary navbar-info navbar-pink";
    var sidebar_classes = "sidebar-light sidebar-dark";
    var $body = $("body");

    //sidebar backgrounds
    $("#sidebar-light-theme").on("click" , function(){
      $body.removeClass(sidebar_classes);
      $body.addClass("sidebar-light");
      $(".sidebar-bg-options").removeClass("selected");
      $(this).addClass("selected");
    });
    $("#sidebar-dark-theme").on("click" , function(){
      $body.removeClass(sidebar_classes);
      $body.addClass("sidebar-dark");
      $(".sidebar-bg-options").removeClass("selected");
      $(this).addClass("selected");
    });


    //Navbar Backgrounds
    $(".tiles.primary").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-primary");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.success").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-success");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.warning").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-warning");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.danger").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-danger");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.light").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-light");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.info").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-info");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.dark").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".navbar").addClass("navbar-dark");
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
    $(".tiles.default").on("click" , function(){
      $(".navbar").removeClass(navbar_classes);
      $(".tiles").removeClass("selected");
      $(this).addClass("selected");
    });
  });
})(jQuery);


// Get references to the elements
const toggleSidebarButton = document.getElementById("toggle-sidebar");
const sidebarBox = document.getElementById("sidebar-box");

// Add a click event listener to the toggle button
toggleSidebarButton.addEventListener("click", function() {
  if (sidebarBox.style.display === "none") {
    sidebarBox.style.display = "block";
  } else {
    sidebarBox.style.display = "none";
  }
});

function changeRowCount(value) {
  var tableRows = document.querySelectorAll('#myTable tbody tr');

  tableRows.forEach(function(row, index) {
    if (value === 'all' || index < value) {
      row.style.display = '';
    } else {
      row.style.display = 'none';
    }
  });
}

// Show default number of rows initially
var defaultRowCount = 5;
var showRowsSelect = document.getElementById('showRows');
showRowsSelect.value = defaultRowCount;
changeRowCount(defaultRowCount);

// Event listener for the "Show rows" dropdown
showRowsSelect.addEventListener('change', function() {
  var selectedValue = showRowsSelect.value;
  changeRowCount(selectedValue);
});



window.addEventListener('resize', function() {
  var width = window.innerWidth;
  var logoImg = document.querySelector('.navbar .navbar-brand-wrapper .brand-logo-mini img');

  if (width < 992) {
    logoImg.style.marginLeft = '0px';
  } else {
    logoImg.style.marginLeft = '15px';
  }
});


// document.getElementById('customFileLabel').addEventListener('click', function() {
//   document.getElementById('file-upload').click();
// });