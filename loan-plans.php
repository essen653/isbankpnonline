<?php

session_start();
if (!isset($_SESSION['email'])) {
    header("Location: http://app.Isbrkonline.com/login.html");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en" itemscope="" itemtype="http://schema.org/WebPage">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Işbank | Turkey's First Bank - Loan Plans</title>
    <meta name="title" content="Işbank | Turkey&#39;s First Bank - Loan Plans">
    <meta name="description"
        content="Isrbank is a complete e-Banking system. We have account-holders from almost all over the world. This is getting popular day by day. Our system is secure and robust. You may feel safe about your deposited funds.">
    <meta name="keywords" content="bank,e-banking,digital banking,digital bank,laon,deposit,fdr,dps">
    <link rel="shortcut icon" href="https://isrbnk.com/assets/images/logoIcon/favicon.png" type="image/x-icon">


    <link rel="apple-touch-icon" href="./assets/logo.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Işbank | Turkey&#39;s First Bank - Loan Plans">

    <meta itemprop="name" content="Işbank | Turkey&#39;s First Bank - Loan Plans">
    <meta itemprop="description" content="">
    <meta itemprop="image" content="https://isrbnk.com/assets/images/seo/64c956e3b38dc1690916579.png">

    <meta property="og:type" content="website">
    <meta property="og:title" content="Isrbank">
    <meta property="og:description"
        content="Isrbank  is a complete e-Banking system. We have account-holders from almost all over the world. This is getting popular day by day. Our system is secure and robust. You may feel safe about your deposited funds.">
    <meta property="og:image" content="https://isrbnk.com/assets/images/seo/64c956e3b38dc1690916579.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1180">
    <meta property="og:image:height" content="600">
    <meta property="og:url" content="loan-plans.php">

    <meta name="twitter:card" content="summary_large_image">

    <link href="./assets/bootstrap.min.css" rel="stylesheet">
    <link href="./assets/all.min.css" rel="stylesheet">
    <link href="./assets/line-awesome.min.css" rel="stylesheet">
    <link href="./assets/slick.css" rel="stylesheet">
    <link href="./assets/lightcase.css" rel="stylesheet">
    <link href="./assets/main.css" rel="stylesheet">
    <link href="./assets/custom.css" rel="stylesheet">
    <link href="./assets/color.php" rel="stylesheet">
</head>

<body>

    <div class="preloader" style="opacity: 0; display: none;">
        <div class="dl">
            <div class="dl__container">
                <div class="dl__corner--top"></div>
                <div class="dl__corner--bottom"></div>
            </div>
            <div class="dl__square"></div>
        </div>
    </div>

    <div class="main-wrapper" style="min-height: calc(100vh - 603px);">
        <header class="header">
            <div class="header__bottom">
                <div class="container">
                    <nav class="navbar navbar-expand-lg align-items-center justify-content-between p-0">
                        <a class="site-logo site-title" href="dashboard.php">
                            <img src="./assets/logo.png" alt="logo">
                        </a>
                        <button class="navbar-toggler" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" type="button"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="menu-toggle"></span>
                        </button>
                        <div class="collapse navbar-collapse mt-xl-0 mt-3" id="navbarSupportedContent">

                            <ul class="navbar-nav main-menu m-auto" id="linkItem">
                                <li><a class="" href="dashboard.php">Dashboard</a></li>

                                <li> <a class="" href="history.php">Deposit</a></li>


                                <li><a class="" href="plans.php">FDR</a></li>

                                <li><a class="" href="dps-plans.php">DPS</a></li>

                                <li><a class="active" href="loan-plans.php">Loan</a></li>

                                <li>
                                    <a class=""
                                        href="transfer.php">Transfer</a>
                                </li>
                            </ul>

                            <div class="nav-right">


                                <a class="btn btn-sm custom--bg py-2 text-white" style="background-color: brown;"
                                href="php/logout.php">Logout</a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <div class="main-wrapper">
            <section class="inner-hero bg_img overlay--one"
                style="background-image: url(&#39;https://isrbnk.com/assets/images/frontend/breadcumb/60c7569dec4f01623676573.jpg&#39;);">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2 class="page-title text-white">Loan Plans</h2>
                        </div>
                    </div>
                </div>
            </section>
            <div class="section--bg2 p-2 bottom-menu-section">
                <div class="container">
                    <nav class="navbar navbar-expand-lg  bottom-menu p-0">
                        <div class="container-lg">
                            <button class="navbar-toggler text-white align-items-center py-2 ms-auto" type="button"
                                data-bs-toggle="collapse" data-bs-target="#bottomMenu" aria-controls="bottomMenu"
                                aria-expanded="false" aria-label="Toggle navigation">
                                <p class="d-flex align-items-center"><span class="fs--14px me-2"></span><i
                                        class="las la-bars"></i></p>
                            </button>
                            <div class="collapse navbar-collapse justify-content-center" id="bottomMenu">
                                <ul class="navbar-nav text-center">
                                    <li><a href="loan-plans.php" class="active">Loan Plans</a></li>
                                    <li><a href="loan-list.php">My Loan List</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>

            <section class="pt-80 pb-80 bg_img"
                style="background-image: url(&#39; https://isrbnk.com/assets/templates/basic/images/elements/bg1.jpg &#39;);">
                <div class="container">
                    <div class="plan-area">
                        <div class="row justify-content-center gy-4 gx-sm-2 gx-md-4">
                        </div>


                    </div>
                </div>
            </section>
            <footer class="footer position-relative z-index-2">
                <div class="container">
                    <!-- <div class="footer__bottom"> -->
                        <div class="row gy-4 align-items-center">
                            <div class="col-lg-3 col-sm-6 order-lg-1 text-sm-start order-1 text-center">
                                <a class="footer-logo" href="#"><img  src="./assets/logo.png" alt="logo"></a>
                            </div>
                            <div class="col-lg-9 col-sm-6 order-lg-3 text-sm-end order-2 text-center">
                                <p>Copyright © 2023  Işbank All Right Reserved</p>
                            </div>
                        </div>
                    <!-- </div> -->
                </div>
            </footer>


        </div>
    </div>

    <div class="modal fade" id="loanModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="loan-plans.php" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title method-name" id="exampleModalLabel">Apply for Loan</h5>
                        <span type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <i class="las la-times"></i>
                        </span>
                    </div>

                    <input type="hidden" name="_token" value="qjnTcfobKoP28Op5zvhqNOyXG3sJBP3gGwOv3i7l">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="" class="required">Amount</label>
                            <div class="input-group">
                                <input type="number" step="any" name="amount" class="form--control"
                                    placeholder="Enter An Amount" required="">
                                <span class="input-group-text"> USD </span>
                            </div>
                            <p><small class="text--danger min-limit"></small></p>
                            <p><small class="text-danger max-limit"></small></p>
                        </div>
                        <button type="submit" class="btn btn--base w-100">Confirm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="./assets/jquery-3.6.0.min.js.download"></script>
    <script src="./assets/bootstrap.bundle.min.js.download"></script>
    <script src="./assets/lightcase.js.download"></script>
    <script src="./assets/slick.min.js.download"></script>
    <script src="./assets/wow.min.js.download"></script>
    <script src="./assets/jquery.validate.js.download"></script>
    <script src="./assets/main.js.download"></script>


    <script>
        (function ($) {
            "use strict";
            $('.loanBtn').on('click', (e) => {
                var modal = $('#loanModal');
                let data = e.currentTarget.dataset;
                modal.find('.min-limit').text(`Minimum Amount ${data.minimum}`);
                modal.find('.max-limit').text(`Maximum Amount ${data.maximum}`);
                let form = modal.find('form')[0];
                form.action = `https://isrbnk.com/user/loan/apply/${data.id}`;
                modal.modal('show');
            });
        })(jQuery);
    </script>
    <script>
        let removeMenu = () => {
            if ($('.bottom-menu ul li').length == 0) {
                $('.bottom-menu-section').remove();
            }
        };
        removeMenu();
    </script>


    <script>
        "use strict";
        (function ($) {

            $(".langSel").on("change", function () {
                window.location.href = "https://isrbnk.com/change/" + $(this).val();
            });

            setTimeout(function () {
                $('.cookies-card').removeClass('hide')
            }, 2000);

            $('.policy').on('click', function () {
                $.get(`https://isrbnk.com/cookie/accept`, function (response) {
                    $('.cookies-card').addClass('d-none');
                });
            });

            $('form').on('submit', function () {
                if ($(this).valid()) {
                    $(':submit', this).attr('disabled', 'disabled');
                }
            });

            var inputElements = $('[type=text],[type=password],select,textarea');

            $.each(inputElements, function (index, element) {
                element = $(element);
                element.closest('.form-group').find('label').attr('for', element.attr('name'));
                element.attr('id', element.attr('name'))
            });

            $.each($('input, select, textarea'), function (i, element) {
                var elementType = $(element);
                if (elementType.attr('type') != 'checkbox' && element.hasAttribute('required')) {
                    $(element).closest('.form-group').find('label').addClass('required');
                }
            });

            $('.showFilterBtn').on('click', function () {
                $('.responsive-filter-card').slideToggle();
            });

            Array.from(document.querySelectorAll('table')).forEach(table => {
                let heading = table.querySelectorAll('thead tr th');
                Array.from(table.querySelectorAll('tbody tr')).forEach((row) => {
                    Array.from(row.querySelectorAll('td')).forEach((colum, i) => {
                        colum.setAttribute('data-label', heading[i].innerText)
                    });
                });
            });

        })(jQuery);
    </script>
    <link rel="stylesheet" href="./assets/iziToast.min.css">
    <script src="./assets/iziToast.min.js.download"></script>

    <script>
        "use strict";

        function notify(status, message) {
            iziToast[status]({
                message: message,
                position: "topRight"
            });
        }
    </script>



</body>

</html>