<?php
session_start();
include "webfonts/config.php";

function validate($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['email']) && isset($_POST['password'])) {
        $email = validate($_POST['email']);
        $pass = validate($_POST['password']);

        $sql1 = "SELECT * FROM `admin` WHERE email = ?";
        $stmt1 = $conn->prepare($sql1);
        $stmt1->bind_param("s", $email);
        $stmt1->execute();
        $result1 = $stmt1->get_result();
        if ($result1->num_rows == 1) {
            $row1 = $result1->fetch_assoc();
            if ($pass === $row1['password']) {
                $_SESSION['phone'] = $row1['phone'];
                $_SESSION['firstname'] = $row1['firstname'];
                $_SESSION['email'] = $row1['email'];
                $_SESSION['id'] = $row1['id'];
                echo "Admin logged in";
            } else {
                echo "Email or password is incorrect";
            }
        } else {
            $sql = "SELECT * FROM `users` WHERE email = ?";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $result = $stmt->get_result();

            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();
                if ($pass === $row['password']) {
                    $_SESSION['email'] = $row['email'];
                    echo "Login successful";
                } else {
                    echo "Email or password is incorrect";
                }
            } else {
                echo "Email or password is incorrect";
            }
        }
    }
}
?>
