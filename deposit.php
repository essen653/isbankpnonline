<?php

session_start();
if (!isset($_SESSION['email'])) {
    header("Location: http://app.Isbrkonline.com/login.html");
    exit();
}
?>  
<!DOCTYPE html>
<html lang="en" itemscope="" itemtype="http://schema.org/WebPage">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Işbank | Turkey's First Bank - Deposit Methods</title>
    <meta name="title" content="Işbank | Turkey&#39;s First Bank - Deposit Methods">
    <meta name="description"
        content="Isrbank is a complete e-Banking system. We have account-holders from almost all over the world. This is getting popular day by day. Our system is secure and robust. You may feel safe about your deposited funds.">
    <meta name="keywords" content="bank,e-banking,digital banking,digital bank,laon,deposit,fdr,dps">
    <link rel="shortcut icon" href="https://isrbnk.com/assets/images/logoIcon/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" href="./assets/logo.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Işbank | Turkey&#39;s First Bank - Deposit Methods">
    <meta itemprop="name" content="Işbank | Turkey&#39;s First Bank - Deposit Methods">
    <meta itemprop="description" content="">
    <meta itemprop="image" content="https://isrbnk.com/assets/images/seo/64c956e3b38dc1690916579.png">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Isrbank">
    <meta property="og:description" content="Isrbank  is a complete e-Banking system. We have account-holders from almost all over the world. This is getting popular day by day. Our system is secure and robust. You may feel safe about your deposited funds.">
    <meta property="og:image" content="https://isrbnk.com/assets/images/seo/64c956e3b38dc1690916579.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1180">
    <meta property="og:image:height" content="600">
    <meta property="og:url" content="deposit.php">
    <meta name="twitter:card" content="summary_large_image">
    <link href="./assets/bootstrap.min.css" rel="stylesheet">
    <link href="./assets/all.min.css" rel="stylesheet">
    <link href="./assets/line-awesome.min.css" rel="stylesheet">
    <link href="./assets/slick.css" rel="stylesheet">
    <link href="./assets/lightcase.css" rel="stylesheet">
    <link href="./assets/main.css" rel="stylesheet">
    <link href="./assets/custom.css" rel="stylesheet">
    <link href="./assets/color.php" rel="stylesheet">
</head>

<body>
    <div class="preloader" style="opacity: 0; display: none;">
        <div class="dl">
            <div class="dl__container">
                <div class="dl__corner--top"></div>
                <div class="dl__corner--bottom"></div>
            </div>
            <div class="dl__square"></div>
        </div>
    </div>
    <div class="main-wrapper" style="min-height: calc(100vh - 603px);">
        <header class="header">
            <div class="header__bottom">
                <div class="container">
                    <nav class="navbar navbar-expand-lg align-items-center justify-content-between p-0">
                        <a class="site-logo site-title" href="dashboard.php">
                            <img src="./assets/logo.png" alt="logo">
                        </a>
                        <button class="navbar-toggler" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" type="button"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="menu-toggle"></span>
                        </button>
                        <div class="collapse navbar-collapse mt-xl-0 mt-3" id="navbarSupportedContent">

                            <ul class="navbar-nav main-menu m-auto" id="linkItem">
                                <li><a class="" href="dashboard.php">Dashboard</a></li>

                                <li> <a class="active" href="history.php">Deposit</a></li>


                                <li><a class="" href="plans.php">FDR</a></li>

                                <li><a class="" href="dps-plans.php">DPS</a></li>

                                <li><a class="" href="loan-plans.php">Loan</a></li>

                                <li>
                                    <a class=""
                                        href="transfer.php">Transfer</a>
                                </li>
                            </ul>

                            <div class="nav-right">
                                <a class="btn btn-sm custom--bg py-2 text-white" style="background-color: brown;"
                                href="php/logout.php">Logout
                                </a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <div class="main-wrapper">
            <section class="inner-hero bg_img overlay--one"
                style="background-image: url(&#39;https://isrbnk.com/assets/images/frontend/breadcumb/60c7569dec4f01623676573.jpg&#39;);">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2 class="page-title text-white">Deposit Methods</h2>
                        </div>
                    </div>
                </div>
            </section>
            <section class="pt-80 pb-80 bg_img"
                style="background-image: url(&#39; https://isrbnk.com/assets/templates/basic/images/elements/bg1.jpg &#39;);">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <form method="post">
                                <input type="hidden" name="_token" value="qjnTcfobKoP28Op5zvhqNOyXG3sJBP3gGwOv3i7l">
                                <input type="hidden" name="method_code">
                                <input type="hidden" name="currency">
                                <div class="card custom--card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label class="form-label required" for="gateway">Select Gateway</label>
                                            <select class="form--control" name="gateway" required="" id="gateway">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label required">Amount</label>
                                            <div class="input-group">
                                                <input type="number" step="any" name="amount" class="form--control"
                                                    value="" autocomplete="off" required="">
                                                <span class="input-group-text">USD</span>
                                            </div>
                                        </div>
                                        <div class="mt-3 preview-details">
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item d-flex justify-content-between">
                                                    <span class="text-muted">Limit</span>
                                                    <span><span class="min ">0</span> USD - <span class="max ">0</span>
                                                        USD</span>
                                                </li>
                                                <li class="list-group-item d-flex justify-content-between">
                                                    <span class="text-muted">Charge</span>
                                                    <span><span class="charge ">0</span> USD</span>
                                                </li>
                                                <li class="list-group-item d-flex justify-content-between">
                                                    <span class="text-muted">Payable</span> <span><span
                                                            class="payable "> 0</span> USD</span>
                                                </li>
                                                <li class="list-group-item justify-content-between d-none rate-element">

                                                </li>
                                                <li class="list-group-item justify-content-between d-none in-site-cur">
                                                    <span class="text-muted">In <span
                                                            class="method_currency"></span></span>
                                                    <span class="final_amo ">0</span>
                                                </li>
                                                <li
                                                    class="list-group-item justify-content-center crypto_currency d-none">
                                                    <span class="text-muted">Conversion with <span
                                                            class="method_currency"></span> and final value will Show on
                                                        next step</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <button type="submit" class="btn btn--base w-100 mt-3">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <footer class="footer position-relative z-index-2">
                <div class="container">
                    <!-- <div class="footer__bottom"> -->
                        <div class="row gy-4 align-items-center">
                            <div class="col-lg-3 col-sm-6 order-lg-1 text-sm-start order-1 text-center">
                                <a class="footer-logo" href="#"><img  src="./assets/logo.png" alt="logo"></a>
                            </div>
                            <div class="col-lg-9 col-sm-6 order-lg-3 text-sm-end order-2 text-center">
                                <p>Copyright © 2023  Işbank All Right Reserved</p>
                            </div>
                        </div>
                    <!-- </div> -->
                </div>
            </footer>


        </div>
    </div>

    <script src="./assets/jquery-3.6.0.min.js.download"></script>
    <script src="./assets/bootstrap.bundle.min.js.download"></script>
    <script src="./assets/lightcase.js.download"></script>
    <script src="./assets/slick.min.js.download"></script>
    <script src="./assets/wow.min.js.download"></script>
    <script src="./assets/jquery.validate.js.download"></script>
    <script src="./assets/main.js.download"></script>


    <script>
        "use strict";
        (function ($) {
            $('select[name=gateway]').change(function () {

                var resource = $('select[name=gateway] option:selected').data('gateway');
                var fixed_charge = parseFloat(resource.fixed_charge);
                var percent_charge = parseFloat(resource.percent_charge);
                var rate = parseFloat(resource.rate)
                if (resource.method.crypto == 1) {
                    var toFixedDigit = 8;
                    $('.crypto_currency').removeClass('d-none');
                } else {
                    var toFixedDigit = 2;
                    $('.crypto_currency').addClass('d-none');
                }
                $('.min').text(parseFloat(resource.min_amount).toFixed(2));
                $('.max').text(parseFloat(resource.max_amount).toFixed(2));
                var amount = parseFloat($('input[name=amount]').val());
                if (!amount) {
                    amount = 0;
                }

                var charge = parseFloat(fixed_charge + (amount * percent_charge / 100)).toFixed(2);
                $('.charge').text(charge);
                var payable = parseFloat((parseFloat(amount) + parseFloat(charge))).toFixed(2);
                $('.payable').text(payable);
                var final_amo = (parseFloat((parseFloat(amount) + parseFloat(charge))) * rate).toFixed(toFixedDigit);
                $('.final_amo').text(final_amo);
                if (resource.currency != 'USD') {
                    var rateElement = `<span class="fw-bold">Conversion Rate</span> <span><span  class="fw-bold">1 USD = <span class="rate">${rate}</span>  <span class="method_currency">${resource.currency}</span></span></span>`;
                    $('.rate-element').html(rateElement);
                    $('.rate-element').removeClass('d-none');
                    $('.in-site-cur').removeClass('d-none');
                    $('.rate-element').addClass('d-flex');
                    $('.in-site-cur').addClass('d-flex');
                } else {
                    $('.rate-element').html('')
                    $('.rate-element').addClass('d-none');
                    $('.in-site-cur').addClass('d-none');
                    $('.rate-element').removeClass('d-flex');
                    $('.in-site-cur').removeClass('d-flex');
                }
                $('.base-currency').text(resource.currency);
                $('.method_currency').text(resource.currency);
                $('input[name=currency]').val(resource.currency);
                $('input[name=method_code]').val(resource.method_code);
                $('input[name=amount]').on('input');
            });
            $('input[name=amount]').on('input', function () {
                $('select[name=gateway]').change();
                $('.amount').text(parseFloat($(this).val()).toFixed(2));
            });
        })(jQuery);
    </script>
    <script>
        let removeMenu = () => {
            if ($('.bottom-menu ul li').length == 0) {
                $('.bottom-menu-section').remove();
            }
        };
        removeMenu();
    </script>


    <script>
        "use strict";
        (function ($) {

            $(".langSel").on("change", function () {
                window.location.href = "https://isrbnk.com/change/" + $(this).val();
            });

            setTimeout(function () {
                $('.cookies-card').removeClass('hide')
            }, 2000);

            $('.policy').on('click', function () {
                $.get(`https://isrbnk.com/cookie/accept`, function (response) {
                    $('.cookies-card').addClass('d-none');
                });
            });

            $('form').on('submit', function () {
                if ($(this).valid()) {
                    $(':submit', this).attr('disabled', 'disabled');
                }
            });

            var inputElements = $('[type=text],[type=password],select,textarea');

            $.each(inputElements, function (index, element) {
                element = $(element);
                element.closest('.form-group').find('label').attr('for', element.attr('name'));
                element.attr('id', element.attr('name'))
            });

            $.each($('input, select, textarea'), function (i, element) {
                var elementType = $(element);
                if (elementType.attr('type') != 'checkbox' && element.hasAttribute('required')) {
                    $(element).closest('.form-group').find('label').addClass('required');
                }
            });

            $('.showFilterBtn').on('click', function () {
                $('.responsive-filter-card').slideToggle();
            });

            Array.from(document.querySelectorAll('table')).forEach(table => {
                let heading = table.querySelectorAll('thead tr th');
                Array.from(table.querySelectorAll('tbody tr')).forEach((row) => {
                    Array.from(row.querySelectorAll('td')).forEach((colum, i) => {
                        colum.setAttribute('data-label', heading[i].innerText)
                    });
                });
            });

        })(jQuery);
    </script>
    <link rel="stylesheet" href="./assets/iziToast.min.css">
    <script src="./assets/iziToast.min.js.download"></script>

    <script>
        "use strict";
        function notify(status, message) {
            iziToast[status]({
                message: message,
                position: "topRight"
            });
        }
    </script>



</body>

</html>